const unoConfig = {
  // rules: [
  //   // custom rules...
  // ],
  // presets: [
  //   // custom presets...
  // ],
  shortcuts: [
    {
      'frow-g2': 'flex flex-row justify-between gap-2',
      'fcol-g2': 'flex flex-col justify-start gap-2',
      btn: 'py-2 px-4 font-semibold rounded-lg shadow-md',
    },
    // dynamic shortcuts:
    // [/^btn-(.*)$/, ([, c]) => `bg-${c}-400 text-${c}-100 py-2 px-4 rounded-lg`],
  ],
}

const getLib = async (libName, libURL, preInitialize = false) => {
  if (!globalThis[libName]) {
    if (preInitialize) globalThis[libName] = null
    const libImport = await import(libURL)
    if (!globalThis[libName]) globalThis[libName] = libImport
  }
  return globalThis[libName]
}

const getLibMap = async ({ libName, libURL, beforeFx, afterFx, preInitialize = false }) => {
  if (!libName && !libURL) return console.warn('what lib do you want actually', libName, libURL)
  if (beforeFx && typeof beforeFx === 'function') await beforeFx()
  if (!globalThis[libName]) {
    if (preInitialize) globalThis[libName] = null
    const libImport = await import(libURL)
    if (!globalThis[libName]) globalThis[libName] = libImport // only assign if the lib did NOT already add itself to globalThis
  }
  if (afterFx && typeof afterFx === 'function') await afterFx()
  return globalThis[libName]
}
const initMap = {
  paraBefore: [
    {
      libName: 'unocss',
      libURL: 'https://cdn.jsdelivr.net/npm/@unocss/runtime/attributify.global.js',
      beforeFx: () => {
        globalThis.__unocss = unoConfig
      },
    },
    // {
    //   beforeFx: () => {
    //     globalThis.require = console.log
    //     globalThis.require = console.log
    //   },
    //   libName: 'bufferMod',
    //   libURL: 'https://cdn.jsdelivr.net/npm/buffer@6.0.3/index.min.js', // requires require
    //   // libURL: 'https://bundle.run/buffer@6.0.3',
    //   afterFx: () => {
    //     console.log({ bufferMod: globalThis.bufferMod, bufInst: globalThis.bufferMod(), gtBuffer: globalThis.Buffer })
    //     // globalThis.Buffer = globalThis.bufferMod.Buffer
    //   },
    // },

  ],
  sequential: [
    {
      libName: 'hybrids',
      libURL: 'https://cdn.skypack.dev/-/hybrids@v8.0.9-uYn92LhWINo1BJzQ1GuC/dist=es2019,mode=imports,min/optimized/hybrids.js',
    },
    {
      libName: 'wn',
      libURL: 'https://unpkg.com/webnative@0.33.0-alpha-1/dist/index.umd.min.js',
    },
    // {
    //   libName: 'sqlwasm',
    //   libURL: 'https://cdnjs.cloudflare.com/ajax/libs/sql.js/1.7.0/sql-wasm-debug.wasm',
    // },
    // {
    //   libName: 'sqljsWorkerModule',
    //   libURL: './worker.sql-wasm-debug.js',
    //   afterFx: () => {
    //     globalThis.sqljsWorker = new Worker('https://cdnjs.cloudflare.com/ajax/libs/sql.js/1.7.0/worker.sql-wasm-debug.js')
    //     console.log({ worker: globalThis.sqljsWorker }, globalThis.initSqlJs)
    //   },
    // },
    {
      // beforeFx: () => {
      //   globalThis.initSqlJs = null
      // },
      libName: 'sqljs',
      libURL: 'https://cdnjs.cloudflare.com/ajax/libs/sql.js/1.7.0/sql-wasm-debug.js',
      afterFx: async () => {
        let { sqljs, initSqlJs } = globalThis
        console.log({ sqljs, initSqlJs })
        const config = {
          locateFile: (filename) => `https://cdnjs.cloudflare.com/ajax/libs/sql.js/1.7.0/${filename}`, // eslint-disable-line @typescript-eslint/restrict-template-expressions
        }

        if (typeof initSqlJs !== 'function') {
          initSqlJs = async function initSqlJs (cfg) {
            // Get the SqlJs code as a string.
            const response = await fetch('https://cdnjs.cloudflare.com/ajax/libs/sql.js/1.7.0/sql-wasm-debug.js')
            const code = await response.text()

            // Instantiate the code and access its exports.
            const f = new Function('exports', code)
            const exports = {}
            f(exports)

            // Call the real initSqlJs().
            return exports.Module(cfg)
          }
        }
        // The `initSqlJs` function is globally provided by all of the main dist files if loaded in the browser.
        // We must specify this locateFile function if we are loading a wasm file from anywhere other than the current html page's folder.

        const SQL = await initSqlJs(config)
        globalThis.SQL = SQL
        // Create the database
        const db = globalThis.SQLdb = new SQL.Database()

        db.run(`CREATE TABLE IF NOT EXISTS txs(
            ts, 
            un, 
            en,
            at, 
            vl,
            PRIMARY KEY(ts,un,en,at)
          );`)
        // id TEXT GENERATED ALWAYS AS (ts || '_' || un) VIRTUAL UNIQUE,
      },
    },
    {
      libName: 'index',
      libURL: './components/index.js',
    },
  ],
  paraAfter: [
    {
      libName: 'datascript',
      libURL: 'https://cdn.jsdelivr.net/npm/datascript@1.3.13/datascript.min.js',
      preInitialize: true,
    },
  ],
}
void (async function init () {
  const paraBeforePromises = []
  const paraAfterPromises = []
  for (const eachPara of initMap.paraBefore) paraBeforePromises.push(getLibMap(eachPara))
  for (const eachPara of initMap.sequential) await getLibMap(eachPara)
  console.log('sequential libs have arrived')
  for (const eachPara of initMap.paraAfter) paraAfterPromises.push(getLibMap(eachPara)) // after all sequentials arrive, but not befores have not necessarily arrived

  void Promise.all(paraBeforePromises).then(() => console.log('before libs have arrived'))
  void Promise.all(paraAfterPromises).then(() => console.log('after libs have arrived', globalThis))

  // load deps from edge
  // await getLib('hybrids', 'https://cdn.skypack.dev/-/hybrids@v8.0.9-uYn92LhWINo1BJzQ1GuC/dist=es2019,mode=imports,min/optimized/hybrids.js')
  // await getLib('wn', 'https://unpkg.com/webnative@0.32.0/dist/index.umd.min.js')
  // await getLib('wn', 'https://unpkg.com/webnative@0.33.0-alpha-1/dist/index.umd.min.js')
  // await getLib('datascript', 'https://cdn.jsdelivr.net/npm/datascript@1.3.13/datascript.min.js', true)

  // load local components
  // await import('./components/index.js')

  // configure and load stylizer
  // globalThis.__unocss = unoConfig
  // await getLib('unocss', 'https://cdn.jsdelivr.net/npm/@unocss/runtime/attributify.global.js')

  // const { __unocss_runtime: unoRuntime } = globalThis
  // console.log({unoRuntime, initUnocssRuntime, globalThis})

  /**
     * TODO try:
     * https://github.com/unocss/unocss/tree/main/packages/preset-icons/
     */
})()

// export { getLib }
