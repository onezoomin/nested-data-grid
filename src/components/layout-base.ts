import type { Component } from 'hybrids'

// expecting these to be imported elsewhere
// import { FlexCol } from './flex-col';
// import { FlexRow } from './flex-row';

let { define, html } = globalThis?.hybrids ?? {} // import { define,html } from 'hybrids'; // will be async imported

interface LayoutBase extends HTMLElement {
  class: any
}
const classDefaultsConnector = (defaultClassesArray: string[]) => ({
  value: defaultClassesArray.join(' '),
  connect: host => host.classList.add(...defaultClassesArray),
})

const classDefaults = ['frow-g2']
const childrenClassAdditions = 'b-1 p-0'
const LayoutBase: Component<LayoutBase> = {
  tag: 'layout-base',

  class: classDefaultsConnector(classDefaults),
  content: (host: LayoutBase) => html`
    <flex-row un-cloak bg="purple-300" classafter="gap-2 w-screen h-screen justify-around m-0 p-3">
        <flex-col un-cloak class="gap-2 grow min-w-1/2 p-0">
            <span bg="blue-600" class="block text-center grow p-2 max-h-10">
                Col Header
            </span>
            <flex-col un-cloak class="gap-2 grow p-2">
                <span class="text-left">
                  data driven...
                </span>
            </flex-col>
        </flex-col>
        <data-col un-cloak class="gap-2 grow min-w-1/2 justify-around">

        </data-col>
    </flex-row>
  `,
}

if (globalThis.hybrids) {
  define(LayoutBase)
}

void (async () => {
  // console.log(define === globalThis.hybrids?.define, {define, globalThis},'before' )
  if (!globalThis.hybrids) {
    const url = 'https://cdn.skypack.dev/-/hybrids@v8.0.9-uYn92LhWINo1BJzQ1GuC/dist=es2019,mode=imports,min/optimized/hybrids.js'
    globalThis.hybrids = await import(url)
    console.log('fetch needed in flex-row')
    ; ({ define, html } = globalThis.hybrids) // destructuring onto pre initialized vars needs this funny ;(syntax) // https://flaviocopes.com/javascript-destructure-object-to-existing-variable/
    // console.log({define, globalThis}, define === globalThis.hybrids.define )
    define(LayoutBase)
  }
})()
