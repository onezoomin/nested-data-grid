import type { Component } from 'hybrids'
const { define, html } = globalThis?.hybrids ?? {} // import { define,html } from 'hybrids'; // will be async imported

interface FlexCol extends HTMLElement {
  class: any
}

const classDefaultsConnector = (defaultClasses: string) => ({
  value: defaultClasses,
  connect: host => {
    // console.log(host.className);
    host.classList.add('|defaults|', ...defaultClasses.split(' '))
    // host.className = `${defaultClasses} | ${host.className}`
    // console.log(host.className);
  },
})

const classDefaults = 'fcol-g2'
const childrenClassAdditions = 'b-1 p-2'
export const FlexCol: Component<FlexCol> = {
  tag: 'flex-col',

  class: classDefaultsConnector(classDefaults),
  content: (host: FlexCol) => {
    const { children } = host
    // console.log('col',{host})
    // host.classList.add(...classDefaults, ...classafter.split(' '))

    return html`
      ${[...children].map(item => {
        // IMPURE manipulating the Element in place:
        // item.classList.add(...childrenClassAdditions.split(' '))
        // console.log({ item, cn: item.className })
        return item
      })}
    
    `
  },
}

if (globalThis.hybrids) {
  define(FlexCol)
} else {
  void getHybridsAndDefine(FlexCol)
}

async function getHybridsAndDefine (ComponentObject) {
  if (!globalThis.hybrids) {
    const hybridsURL = 'https://cdn.skypack.dev/-/hybrids@v8.0.9-uYn92LhWINo1BJzQ1GuC/dist=es2019,mode=imports,min/optimized/hybrids.js'
    globalThis.hybrids = await import(hybridsURL)
    globalThis.hybrids.define(ComponentObject)
  }
}
