import { ColData, Row } from './data'
import type { Component } from 'hybrids'
const { define, html, store } = globalThis?.hybrids ?? {} // import { define,html } from 'hybrids'; // will be async imported

interface DataCol extends HTMLElement {
  class: any
  data?: any
  // mockData?: any;
}

const classDefaultsConnector = (defaultClasses: string) => ({
  value: defaultClasses,
  connect: host => {
    // console.log(host.className);
    host.classList.add('|defaults|', ...defaultClasses.split(' '))
    // host.className = `${defaultClasses} | ${host.className}`
    // console.log(host.className);
  },
})

const classDefaults = 'fcol-g2'
const childrenClassAdditions = 'b-1 p-2'
export const DataCol: Component<DataCol> = {
  tag: 'data-col',
  data: () => store.get(ColData),
  // mockData: { get: ()=>mockData },
  class: classDefaultsConnector(classDefaults),
  content: (host: DataCol) => {
    const { data } = host
    // console.log('datacol',{host})
    // host.classList.add(...classDefaults, ...classafter.split(' '))

    return html`
      ${data.rows.map((eachRow: Row) => {
      // console.log({ els: eachRow.elements })
        return html`
        <flex-row class="min-h-10" >
          ${eachRow.elements.filter(el => !el.isDeleted).map(el => html`
            <div class="rounded p-2 text-xs" style="background-color: ${el.color}">${el.color.toUpperCase()}</div>
          `.key(el.lastUpdate))}
        </flex-row>
        `.key(eachRow)
      })}
    
    `
  },
}

if (globalThis.hybrids) {
  define(DataCol)
} else {
  void getHybridsAndDefine(DataCol)
}

async function getHybridsAndDefine (ComponentObject) {
  if (!globalThis.hybrids) {
    const hybridsURL = 'https://cdn.skypack.dev/-/hybrids@v8.0.9-uYn92LhWINo1BJzQ1GuC/dist=es2019,mode=imports,min/optimized/hybrids.js'
    globalThis.hybrids = await import(hybridsURL)
    globalThis.hybrids.define(ComponentObject)
  }
}
