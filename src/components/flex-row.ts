import type { Component } from 'hybrids'
let { define, html } = globalThis?.hybrids ?? {} // import { define,html } from 'hybrids'; // will be async imported

interface FlexRow extends HTMLElement {
  classafter: string
  class: any
}

const classDefaultsConnector = (defaultClassesArray: string[]) => ({
  value: defaultClassesArray.join(' '),
  connect: host => host.classList.add(...defaultClassesArray, ...host.classafter.split(' ')),
})

const classDefaults = ['frow-g2']
const childrenClassAdditions = 'b-1 p-0'
export const FlexRow: Component<FlexRow> = {
  tag: 'flex-row',

  classafter: 'gap-2', // be aware trumping may not work as you expect it to (based on the order of rule _definition_ NOT application order)

  class: classDefaultsConnector(classDefaults),
  content: (host: FlexRow) => {
    const { children, classafter = '' } = host

    // host.classList.add(...classDefaults.split(' '), ...classafter.split(' '))

    return html`
      ${[...children].map(item => {
      // IMPURE manipulating the Element in place:
      // item.classList.add(...(item['classafter']?.split(' ') ?? []), ...childrenClassAdditions.split(' '))
      // console.log('row',{ item, cn: item.className })
      return html`${item}`
    })}
    `
  },
}

if (globalThis.hybrids) {
  define(FlexRow)
}

void (async () => {
  // console.log(define === globalThis.hybrids?.define, {define, globalThis},'before' )
  if (!globalThis.hybrids) {
    const url = 'https://cdn.skypack.dev/-/hybrids@v8.0.9-uYn92LhWINo1BJzQ1GuC/dist=es2019,mode=imports,min/optimized/hybrids.js'
    globalThis.hybrids = await import(url)
    console.log('fetch needed in flex-row')
    ; ({ define, html } = globalThis.hybrids) // destructuring onto pre initialized vars needs this funny ;(syntax) // https://flaviocopes.com/javascript-destructure-object-to-existing-variable/
    // console.log({define, globalThis}, define === globalThis.hybrids.define )
    define(FlexRow)
  }
})()
