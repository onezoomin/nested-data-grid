void (async () => {
  // load deps from edge
  const hybridsURL = 'https://cdn.skypack.dev/-/hybrids@v8.0.9-uYn92LhWINo1BJzQ1GuC/dist=es2019,mode=imports,min/optimized/hybrids.js'
  globalThis.hybrids = await import(hybridsURL)

  // load local modules
  // await import('./flex-row.js')
  // await import('./flex-col.js')
  // await import('./layout-base.js')

  // load stylizer
  const unocssURL = 'https://cdn.jsdelivr.net/npm/@unocss/runtime/attributify.global.js'
  globalThis.__unocss = {
    // rules: [
    //   // custom rules...
    // ],
    // presets: [
    //   // custom presets...
    // ],
    shortcuts: [
      {
        'frow-g2': 'flex flex-row justify-between gap-2',
        'fcol-g2': 'flex flex-col justify-around gap-2',
        btn: 'py-2 px-4 font-semibold rounded-lg shadow-md',
      },
      // dynamic shortcuts:
      // [/^btn-(.*)$/, ([, c]) => `bg-${c}-400 text-${c}-100 py-2 px-4 rounded-lg`],
    ],
  }

  const initUnocssRuntime = await import(unocssURL)

  const { __unocss_runtime: unoRuntime } = globalThis
  console.log({ unoRuntime, initUnocssRuntime, globalThis })

  /**
     * TODO try:
     * https://github.com/unocss/unocss/tree/main/packages/preset-icons/
     */
})()
