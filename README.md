# Nested Data Driven Grid 

Exploring data driven web-components using:   
 * [Parcel](https://parceljs.org/) for HMR and bundling
 * [Unocss](https://github.com/unocss/unocss) for styling  
 * [Hybrids](https://github.com/hybridsjs/hybrids) for components and reactive data store
 
Preview edge deployment via [fission](https://guide.fission.codes/developers/cli/working-with-apps):  
https://calm-white-metalic-yeti.fission.app/

### Inspired by and forked from:  
[Create your own typescript library with Parcel.js](https://dev.to/ihaback/create-your-own-typescript-library-with-parceljs-3dh7)

# Required
- [Node](https://nodejs.org/en/download/)
- [Parcel](https://parceljs.org/getting-started/library/) - (pnpm takes care of install)

# Install
if needed [install pnpm first](https://pnpm.io/installation)

```
pnpm i
```


# Development

```
pnpm run dev
```

# Build

```
pnpm run build
```


# Lint

```
pnpm run lint
```

# Test

```
pnpm run test
```

# Publish (optional if you have npmjs.com account)

```
npm login
```
```
npm version minor
```
```
npm publish --access public
```
